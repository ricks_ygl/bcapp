## Installation

API is done in Laravel(5.4) framework.Virtualisation is done via Docker.Make sure docker and docker-compose installed.

### Steps:

1.    `git clone https://ricks_ygl@bitbucket.org/ricks_ygl/bcapp.git`

2.    `cd bcapp` (Set enough permissions for the 'bcapp' folder.preferably 777)

3.    `./install.sh` (Make sure the install.sh is executable)

## Post Installation

* After installation the Api will be available in http://localhost:8080
* phpmyadmin is avalable in http://localhost:8081 with credentials 
`Username:bcapp ,
password:bcapp`
* when you are done with changes or testing on project instance run `docker-compose stop && docker-compose rm`(from project root folder) to stop and remove the container.
* Once you had the docker image(After the first installation) you can kickstart(assuming you removed the container) the project at any moment with running  `./install.sh` from project root

###Postman Collection

postman collection is available under `postman` folder in root as `Bcapp.postman_collection.json`. Please do import that postman collection.
Instructions regarding Api endpoints are described along with each Api endpoint in postman collection.The Api requests are authenticated using an 
`'Authorization'` header param with a user token as its value.You can find its details in postman collection doc itself.

###Logging

API requests will be logged onto the default laravel log file `storage/logs/laravel.log`.The API log will start like `---------- API Request ---------------`


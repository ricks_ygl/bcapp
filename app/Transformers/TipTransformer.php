<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class TipTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($tip)
    {
        return [
            'guid' => $tip->guid,
            'title' => $tip->title,
            'description' => $tip->description
        ];
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class LogRequests
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->start = microtime(true);
        return $next($request);
    }
    
    /**
     * terminate
     * @param  \Illuminate\Http\Request  $request
     * @param Illuminate\Http\JsonResponse $response
     * @return void
     */
    public function terminate($request, $response)
    {
        $request->end = microtime(true);
        $this->log($request,$response);
    }
    
    /**
     * logs the details
     * @param  \Illuminate\Http\Request  $request
     * @param Illuminate\Http\JsonResponse $response
     * @return void
     */
    protected function log($request,$response)
    {
        $duration = $request->end - $request->start;
        $url = $request->fullUrl();
        $method = $request->getMethod();
        $ip = $request->getClientIp();
        $log = "\n---------- API Request ----------------\n".
        "IP {$ip}: {$method}@{$url}\n".
        "Duration: {$duration}ms \n".
        "Request params :  ".json_encode($request->all())." \n".
        "Response : {$response->getContent()} \n".
        "---------- API Request Ends -------------\n";
        Log::info($log);
    }
}
